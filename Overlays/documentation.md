players=2_game=928x696_face=576x324
  GAME
    1920
    960 = 1920 / 2 : half
    940 = 960 - 20 : with 10px borders on every side
    930 = 960 - 10 : with 5px padding on each side
    
    game 		= 928x696 : closest 4:3 aspect ratio
    gameBorder 	= 948x716

  FACE
    359 = remaining height without game + border
    339 = 359 - 20 : with 10px borders on every side
    329 = 329 - 10 : with 5px padding on each side
    
    
    face 		= 576x324 : closest 16:9 aspect ratio
    faceBorder 	= 596x344